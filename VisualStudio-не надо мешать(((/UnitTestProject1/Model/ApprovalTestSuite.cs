﻿using System;
using ClassLibrary1.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Model
{

    //создание подтверждения
    [TestClass]
    public class ApprovalTestSuite
    {
        [TestMethod]
        public void ApprovalCreatedWithoutException()
        {
            new Approval(1, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void ApprovalHasNoNullNumber()
        {
            new Approval(0, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = true)]
        public void ApprovalHasNoNullDate()
        {
            new Approval(1, new DateTime(0));
        }

    }
}
