﻿namespace ClassLibrary1.Pattern
{
    public interface UseCase<TRequestModel> where TRequestModel : struct
    {
        void Execute(TRequestModel request);
    }
    public interface UseCase
    {
        void Execute();
    }
}
