﻿using System.Collections.Generic;

namespace ClassLibrary1.Pattern
{
    public interface Repository<TBusinessObject, TKey>
        where TBusinessObject : class
    {
        void Add(TBusinessObject value);
        void Delete(TBusinessObject value);
        IEnumerable<TBusinessObject> GetAll();
        TBusinessObject Get(TKey id);
        TKey GetId(TBusinessObject businessObject);
    }
}
