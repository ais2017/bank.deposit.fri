﻿using System;
namespace ClassLibrary1.Pattern
{
    public interface BusinessTransaction : IDisposable
    {
        //Repository <Model.Company, int> Companies { get; }
        void Save();
    }
}
