﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Model
{
    enum statusReq { Reviewed, NotReviewed };
    //ЗАЯВКА
    public class Request
    {
        DateTime dateRequest;
        int numberRequest;
        //Статус заявки перечисление
        statusReq statusRequest;
        int summa;

        Deposit deposit;
        Approval approval;

        public Request(int num, int dengi, Approval ap)
        {
            dateRequest = DateTime.Now;
            numberRequest = num;
            statusRequest = statusReq.NotReviewed;
            summa = dengi;
            approval = ap;
        }
        //открыть вклад
        public void openDeposit(Deposit dep)
        {
            deposit = dep;
        }

        //получить вклад
        public Deposit getDeposit()
        {
            return deposit;
        }

        //проверка статуса вклада
        public statusDeposit checkStatusDeposit()
        {
            return deposit.statusVklada;
        }
        //получить утверждение
        public Approval getApproval()
        {
            return approval;
        }
    }



}
