﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Model
{

    //ПРОГРАММА
    public class Program
    {
        string nameProgram;
        int rateForPeriod;
        string currency;
        string relevanceStatus;
        int durability;
        int periodValue;
        int minSumDeposit;
        int maxSumDeposit;
        int expirePeriod;
        //ссылка на вклад
        Deposit deposit;

        //создаю методы внутри класса
        //void - ничего не возвращает
        //если возвращает, то public int(или string) ИмяМетода

        //закрыть программу?
        public void CloseApp()
        {
            throw new Exception("Я хотел закрыть программу, но что-то пошло не так");
        }
        //изменить название программы
        public void changeNameProgram(string n)
        {
            nameProgram = n;
        }
        public void changeMinSum(int min)
        {
            minSumDeposit = min;
        }
        public void changeMaxSum(int max)
        {
            maxSumDeposit = max;
        }
        public void changeDurability(int duration)
        {
            durability = duration;
        }
        public void changeRelevanceStatus(string relevStatus)
        {
            relevanceStatus = relevStatus;
        }
        public void changePeriodValue(int periodVal)
        {
            periodValue = periodVal;
        }
        public void changeExpirePeriod(int expirePer)
        {
            expirePeriod = expirePer;
        }

        //получить название программы
        public string GetNameProgram()
        {
            return nameProgram;
        }

        //получение всех вкладов?
        public Deposit[] GetAllDeposits()
        {
            throw new ArgumentException();
        }

        //получение вклада по номеру?
        public Deposit GetDepositByNum(int num)
        {
            throw new ArgumentException();
        }
        //делаем конструктор
        public Program(string name, int rate, string cur, int duration, int period, int minSum, int maxSum, int expire)
        {
            nameProgram = name;
            rateForPeriod = rate;
            currency = cur;
            durability = duration;
            periodValue = period;
            minSumDeposit = minSum;
            maxSumDeposit = maxSum;
            expirePeriod = expire;
        }
    }


}
