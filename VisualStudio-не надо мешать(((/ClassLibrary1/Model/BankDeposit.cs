﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*namespace ClassLibrary1.Model
{
    class BankDeposit
    {
    }
}*/

namespace ClassLibrary1.Model
{
    public enum statusDeposit { Open, Closed };
    //ВКЛАД
    public class Deposit
    {
        public int numberDeposit { get; private set; }
        int sumDeposit;
        int numberWorker;
        DateTime openDateDeposit;
        DateTime closeDateDeposit;
        //перечисление:
        public statusDeposit statusVklada { get; private set; }

        Request request;


        //конструктор
        public Deposit(int num, int sum, int numw, DateTime op, Request req)
        {
            numberDeposit = num;
            sumDeposit = sum;
            numberWorker = numw;
            openDateDeposit = op;
            statusVklada = statusDeposit.Open;
            request = req;
        }
        //закрыть вклад
        public void CloseDeposit()
        {
            closeDateDeposit = DateTime.Now;
            statusVklada = statusDeposit.Closed;
        }
        //вычислить доход за период
        public string CountIncomeForPeriod(DateTime st, DateTime fin)
        {
            return (fin - st).ToString(); //непонятно как считать доход
        }
        //получить заявку
        public Request GetRequest()
        {
            return request;
        }
    }
}