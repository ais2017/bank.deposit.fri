﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Model
{

    //КЛИЕНТ
    public class Client
    {
        string surnameClient;
        string nameClient;
        string patronimicClient;
        int phoneNumberClient;

        Request request;
        Deposit []deposits = new Deposit[50];

        public int getPhoneNumber()
        {
            return phoneNumberClient;
        }

        public string getFIOClient()
        {
            return surnameClient + " " + nameClient + " " + patronimicClient;
        }

        //получение всех вкладов
        public Deposit[] GetAllDeposits()
        {
            return deposits;
        }
        //получение вклада по номеру
        public Deposit getDepositNumber(int Numb)
        {
            if (deposits[Numb] != null)
            {
                return deposits[Numb];
            }
            else throw new ArgumentNullException("Нет такого номера");
        }

        //добавление заявки: Заявка
        public void AddRequest(int num, int sum, Approval ap)
        {
            request = new Request(num, sum, ap);
        }
        //добавление вклада: Вклад
        public void AddDeposit(int num, int sum, int numw, DateTime op, Request req)
        {
            int count = deposits.Length;
            deposits[count] = new Deposit(num, sum, numw, op, req);
        }
        //получение вклада по номеру
        public Deposit getDepositNumber(Deposit dip)
        {
            deposits[dip.numberDeposit] = dip;
            return dip;
        }

        //делаем конструктор добавление номера телефона, ФИО
        public Client(int phone, string name, string patronimic, string surname)
        {
            phoneNumberClient = phone;
            nameClient = name;
            patronimicClient = patronimic;
            surnameClient = surname;
        } } }



