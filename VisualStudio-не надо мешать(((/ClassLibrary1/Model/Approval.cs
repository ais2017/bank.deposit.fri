﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Model
{
    public enum ApprovalStatus { OnRassmotr, Approved, Declined };
    //УТВЕРЖДЕНИЕ
    public class Approval
    {
        DateTime dateApproval;
        int idSpecialist;
        //статус утверждения - перечисление
        ApprovalStatus statusApproval;

        Request request;

        //утвердить статус
        public void AcceptStatus()
        {
            statusApproval = ApprovalStatus.Approved;
        }
        //отклонить статус
        public void DeclineStatus()
        {
            statusApproval = ApprovalStatus.Declined;
        }
        //получить статус из перечисления для переменной "Статус"
        public string getStatusRequest()
        {
            return this.statusApproval.ToString();
        }

        //конструктор (тестируем)
        public Approval(int idSpec, DateTime dat)
        {
            if (idSpec == 0) throw new ArgumentNullException();
            if (dat == new DateTime(0)) throw new ArgumentNullException();

            idSpecialist = idSpec;
            dateApproval = dat;
            statusApproval = ApprovalStatus.OnRassmotr;
            //request = req;
        }



    }
}
